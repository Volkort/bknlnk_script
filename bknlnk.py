#!/usr/bin/env python
# -*- coding: utf-8 -*-

''' Unix command, that look for dead link in any 
url files in workingdirectories recursively.'''

from bs4 import BeautifulSoup
from urllib.parse import urljoin, urlparse
import argparse
import requests

def brklnk(url, depth):
    if depth < 0:
        return
    request = requests.get(url)
    if request.status_code >= 400:
        print(url, ": link is broken")
        return
    soup = BeautifulSoup(request.content, 'html.parser')
    for link in soup.find_all('a'):
        newlink = urlparse(link.get('href'))
        if newlink.scheme == '':
            newlink_correct = urljoin(url, newlink.path)
            brklnk(newlink_correct, depth -1)       


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('url', help = 'Starting website to bo inspected', type= str)
    parser.add_argument('--depth', '-d', help = 'depth of search', type= int, default = 1)
    args = parser.parse_args()
    brklnk(args.url, args.depth)
